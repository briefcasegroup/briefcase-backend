use std::collections::HashMap;
use deadpool_redis::{Config, Pool, Runtime};
use redis::cmd;
use serde::Serialize;
use uuid::Uuid;

use crate::storage::ImageInfo;

static REDIS_SESSIONS_SET: &str = "sessions";
static REDIS_IMAGES_LIST_STR: &str = "images";
static REDIS_IMAGES_QUEUE: &str = "image_queue";
static REDIS_IMAGES_TO_PROCESS: &str = "images_to_process";
static REDIS_IMAGES_PROCESSED: &str = "processed_images";
static REDIS_IMAGES_MAPPINGS: &str = "images_mappings";

pub struct SessionService {
    redis_pool: Pool,
}

impl SessionService {
    pub fn new() -> SessionService {
        let cfg = Config::from_url("redis://localhost/");
        let pool = cfg.create_pool(Some(Runtime::Tokio1)).unwrap();
        SessionService { redis_pool: pool }
    }

    pub async fn create_session(&self) -> Session {
        let new_session = Session { id: Uuid::new_v4() };

        let mut conn = self.redis_pool.get().await.unwrap();
        let _: i32 = cmd("SADD")
            .arg(&[REDIS_SESSIONS_SET, &new_session.id.to_string()])
            .query_async(&mut conn)
            .await
            .unwrap();

        new_session
    }

    pub async fn session_exists(&self, session: &Session) -> bool {
        let mut conn = self.redis_pool.get().await.unwrap();
        let result: i32 = cmd("SISMEMBER")
            .arg(&[REDIS_SESSIONS_SET, &session.id.to_string()])
            .query_async(&mut conn)
            .await
            .unwrap();

        result == 1
    }

    pub async fn upload_image(&self, session: Session, image: ImageInfo) {
        let mut conn = self.redis_pool.get().await.unwrap();
        let list_name = format!("{}:{}", REDIS_IMAGES_LIST_STR, session.id);
        let _: i32 = cmd("LPUSH")
            .arg(&[&list_name, &image.file_name])
            .query_async(&mut conn)
            .await
            .unwrap();

        let image_command = format!("{};{}", session.id.to_string(), &image.file_name);
        let images_to_process_variable_name =
            format!("{}:{}", REDIS_IMAGES_TO_PROCESS, session.id.to_string());
        let _: (i32, i32) = redis::pipe()
            .atomic()
            .cmd("RPUSH")
            .arg(&[REDIS_IMAGES_QUEUE, &image_command])
            .cmd("INCR")
            .arg(&[images_to_process_variable_name])
            .query_async(&mut conn)
            .await
            .unwrap();
    }

    pub async fn check_progress(&self, session: Session) -> ProgressStatus {
        let mut conn = self.redis_pool.get().await.unwrap();
        let to_process_name = format!("{}:{}", REDIS_IMAGES_TO_PROCESS, session.id);
        let to_process_result = cmd("GET")
            .arg(&to_process_name)
            .query_async(&mut conn)
            .await
            .unwrap();
        let mut to_process = 0;
        if let Some(x) = to_process_result {
            to_process = x;
        }
        let processed_name = format!("{}:{}", REDIS_IMAGES_PROCESSED, session.id);
        let processed_result = cmd("GET")
            .arg(&processed_name)
            .query_async(&mut conn)
            .await
            .unwrap();
        let mut processed = 0;
        if let Some(x) = processed_result {
            processed = x
        }
        ProgressStatus {
            to_process,
            processed
        }
    }

    pub async fn get_categorized(&self, session: Session) -> HashMap<String, String> {
        let mut conn = self.redis_pool.get().await.unwrap();
        let images_list_name = format!("{}:{}", REDIS_IMAGES_MAPPINGS, session.id);
        let images_list_result: HashMap<String,String> = cmd("HGETALL")
            .arg(&images_list_name)
            .query_async(&mut conn)
            .await
            .unwrap();
        images_list_result
    }
}

#[derive(Serialize)]
pub struct ProgressStatus {
    to_process: i32,
    processed: i32
}

#[derive(Serialize)]
pub struct Session {
    id: Uuid,
}

impl Session {
    pub fn with_uuid(uuid: Uuid) -> Session {
        Session { id: uuid }
    }
}
