use crate::sessions::{Session, SessionService};
use rocket::{http::Status, serde::json::Json, State};

#[post("/")]
pub async fn batches_post(
    session_service: &State<SessionService>,
) -> Result<Json<Session>, Status> {
    Ok(Json(session_service.create_session().await))
}
