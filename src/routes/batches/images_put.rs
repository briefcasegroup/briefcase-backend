use std::str::FromStr;

use rocket::http::Status;
use rocket::Data;
use rocket::State;
use uuid::Uuid;

use crate::sessions::{Session, SessionService};
use crate::storage::StorageService;

#[put("/<batch_id>/images", format = "image/*", data = "<data>")]
pub async fn batches_images_put(
    batch_id: &str,
    data: Data<'_>,
    session_service: &State<SessionService>,
    storage_service: &State<StorageService>,
) -> Result<Status, Status> {
    let batch_id = Uuid::from_str(batch_id).map_err(|_| Status::BadRequest)?;
    let session = Session::with_uuid(batch_id);
    let session_exists = session_service.session_exists(&session).await;
    if !session_exists {
        return Err(Status::NotFound);
    }

    let image_data = storage_service.save_image(data).await;
    session_service.upload_image(session, image_data).await;
    Ok(Status::Created)
}
