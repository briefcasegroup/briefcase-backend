use std::str::FromStr;
use rocket::http::Status;
use rocket::serde::json::Json;
use rocket::State;
use uuid::Uuid;
use crate::{SessionService, StorageService};
use crate::sessions::{ProgressStatus, Session};

#[get("/<batch_id>/progress")]
pub async fn batches_progress_get(
    batch_id: &str,
    session_service: &State<SessionService>,
) -> Result<Json<ProgressStatus>, Status> {
    let batch_id = Uuid::from_str(batch_id).map_err(|_| Status::BadRequest)?;
    let session = Session::with_uuid(batch_id);
    let session_exists = session_service.session_exists(&session).await;
    if !session_exists {
        return Err(Status::NotFound);
    }

    Ok(Json(session_service.check_progress(session).await))
}