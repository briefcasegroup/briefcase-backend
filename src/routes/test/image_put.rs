use rocket::data::ToByteUnit;
use rocket::http::Status;
use rocket::Data;

#[put("/image", format = "image/*", data = "<data>")]
pub async fn test_image_post(data: Data<'_>) -> Result<String, Status> {
    data.open(10.megabytes())
        .into_file("temp/img")
        .await
        .unwrap();
    Ok("gituwa".parse().unwrap())
    // return Err(Status::Unauthorized);
}
