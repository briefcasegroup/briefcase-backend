mod routes;
mod sessions;
mod storage;

#[macro_use]
extern crate rocket;

use rocket::fs::FileServer;
use routes::batches::_post::batches_post;
use routes::batches::images_get::batches_images_get;
use routes::batches::images_put::batches_images_put;
use routes::batches::progress_get::batches_progress_get;
use routes::test::image_put::test_image_post;
use sessions::SessionService;
use storage::StorageService;

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .manage(SessionService::new())
        .manage(StorageService)
        .mount("/", routes![index])
        .mount(
            "/batches",
            routes![batches_post, batches_images_get, batches_images_put, batches_progress_get],
        )
        .mount("/test", routes![test_image_post])
        .mount("/images", FileServer::from("images"))
}
