use std::path::PathBuf;

use rocket::{data::ToByteUnit, Data};
use uuid::Uuid;

pub struct ImageInfo {
    pub file_name: String,
}

pub struct StorageService;

impl StorageService {
    pub async fn save_image(&self, data: Data<'_>) -> ImageInfo {
        let image_uuid = Uuid::new_v4();
        let mut path = PathBuf::new();
        path.push("images");
        path.push(image_uuid.to_string());
        path.set_extension("jpg");

        data.open(10.megabytes()).into_file(&path).await.unwrap();
        ImageInfo {
            file_name: path.file_name().unwrap().to_str().unwrap().to_owned(),
        }
    }
}
